package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        // Если значения List = null
        if(x==null||y==null) throw new IllegalArgumentException();
        int indexX=0, indexY=0;
        while((indexX!=x.size())&&(indexY!=y.size())){
            if(x.get(indexX) == y.get(indexY)){
                indexX++;
                indexY++;
            }
            else indexY++;
        }
        if(indexX==x.size())
            return true;
        else return false;
    }
}

