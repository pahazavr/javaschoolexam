package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        BigDecimal result = calculateExpression(statement);
        if(result.compareTo(BigDecimal.ZERO) != 0) return roundResult(result.toString());
        else return null;
    }

    /**
     * Основные математические операции и их приоритеты.
     *
     * @see #sortingStation(String, java.util.Map)
     */
    public static final Map<String, Integer> MAIN_MATH_OPERATIONS;

    static {
        MAIN_MATH_OPERATIONS = new HashMap<String, Integer>();
        MAIN_MATH_OPERATIONS.put("*", 1);
        MAIN_MATH_OPERATIONS.put("/", 1);
        MAIN_MATH_OPERATIONS.put("+", 2);
        MAIN_MATH_OPERATIONS.put("-", 2);
    }

    /**
     * Преобразует выражение из инфиксной нотации в обратную польскую нотацию (ОПН) по алгоритму <i>Сортировочная
     * станция</i> Эдскера Дейкстры. Отличительной особенностью обратной польской нотации является то, что все
     * аргументы (или операнды) расположены перед операцией. Это позволяет избавиться от необходимости использования
     * скобок. Например, выражение, записаное в инфиксной нотации как 3 * (4 + 7), будет выглядеть как 3 4 7 + *
     * в ОПН. Символы скобок могут быть изменены.
     *
     * @param expression выражение в инфиксной форме.
     * @param operations операторы, использующиеся в выражении (ассоциированные, либо лево-ассоциированные).
     * Приведенные операторы определены в константе {@link #MAIN_MATH_OPERATIONS}.
     * @param leftBracket открывающая скобка.
     * @param rightBracket закрывающая скобка.
     * @return преобразованное выражение в ОПН.
     */
    public static String sortingStation(String expression, Map<String, Integer> operations, String leftBracket, String rightBracket) {
        if (expression == null || expression.length() == 0 || expression.contains("--") || expression.contains("++") || expression.contains("//") || expression.contains("**") )
            return null;
        if (operations == null || operations.isEmpty())
            return null;
        // Выходная строка, разбитая на "символы" - операции и операнды..
        List<String> out = new ArrayList<String>();
        // Стек операций.
        Stack<String> stack = new Stack<String>();

        // Удаление пробелов из выражения.
        expression = expression.replace(" ", "");

        // Множество "символов", не являющихся операндами (операции и скобки).
        Set<String> operationSymbols = new HashSet<String>(operations.keySet());
        operationSymbols.add(leftBracket);
        operationSymbols.add(rightBracket);

        // Индекс, на котором закончился разбор строки на прошлой итерации.
        int index = 0;
        // Признак необходимости поиска следующего элемента.
        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = expression.length();
            String nextOperation = "";
            // Поиск следующего оператора или скобки.
            for (String operation : operationSymbols) {
                int i = expression.indexOf(operation, index);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }
            // Оператор не найден.
            if (nextOperationIndex == expression.length()) {
                findNext = false;
            } else {
                // Если оператору или скобке предшествует операнд, добавляем его в выходную строку.
                if (index != nextOperationIndex) {
                    out.add(expression.substring(index, nextOperationIndex));
                }
                // Обработка операторов и скобок.
                // Открывающая скобка.
                if (nextOperation.equals(leftBracket)) {
                    stack.push(nextOperation);
                }
                // Закрывающая скобка.
                else if (nextOperation.equals(rightBracket)) {
                    while (!stack.peek().equals(leftBracket)) {
                        out.add(stack.pop());
                        if (stack.empty()) {
                            //throw new IllegalArgumentException("Unmatched brackets");
                            return null;
                        }
                    }
                    stack.pop();
                }
                // Операция.
                else {
                    while (!stack.empty() && !stack.peek().equals(leftBracket) &&
                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
                        out.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationIndex + nextOperation.length();
            }
        }
        // Добавление в выходную строку операндов после последнего операнда.
        if (index != expression.length()) {
            out.add(expression.substring(index));
        }
        // Пробразование выходного списка к выходной строке.
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        StringBuffer result = new StringBuffer();
        if (!out.isEmpty())
            result.append(out.remove(0));
        while (!out.isEmpty())
            result.append(" ").append(out.remove(0));
        return result.toString();
    }

    /**
     * Преобразует выражение из инфиксной нотации в обратную польскую нотацию (ОПН) по алгоритму <i>Сортировочная
     * станция</i> Эдскера Дейкстры. Отличительной особенностью обратной польской нотации является то, что все
     * аргументы (или операнды) расположены перед операцией. Это позволяет избавиться от необходимости использования
     * скобок. Например, выражение, записаное в инфиксной нотации как 3 * (4 + 7), будет выглядеть как 3 4 7 + *
     * в ОПН.
     *
     * @param expression выражение в инфиксной форме.
     * @param operations операторы, использующиеся в выражении (ассоциированные, либо лево-ассоциированные).
     * Приведенные операторы определены в константе {@link #MAIN_MATH_OPERATIONS}.
     * @return преобразованное выражение в ОПН.
     */
    public static String sortingStation(String expression, Map<String, Integer> operations) {
        return sortingStation(expression, operations, "(", ")");
    }

    /**
     * Вычисляет значение выражения, записанного в инфиксной нотации. Выражение может содержать скобки, числа с
     * плавающей точкой, четыре основных математических операндов.
     *
     * @param expression выражение.
     * @return результат вычисления.
     */
    public static BigDecimal calculateExpression(String expression) {
        String rpn = sortingStation(expression, MAIN_MATH_OPERATIONS);
        if(rpn!=null) {
            StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
            Stack<BigDecimal> stack = new Stack<BigDecimal>();
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                if(token.contains(",")) return BigDecimal.valueOf(0);
                // Операнд.
                if (!MAIN_MATH_OPERATIONS.keySet().contains(token)) {
                    if(!isNum(token)) return BigDecimal.valueOf(0);
                    else stack.push(new BigDecimal(token));
                } else {
                    BigDecimal operand2 = stack.pop();
                    BigDecimal operand1 = stack.empty() ? BigDecimal.ZERO : stack.pop();
                    if (token.equals("*")) {
                        stack.push(operand1.multiply(operand2));
                    } else if (token.equals("/")) {
                        if(operand2.compareTo(BigDecimal.ZERO) != 0){
                            stack.push(operand1.divide(operand2, 15, RoundingMode.HALF_EVEN));
                        }
                        else return BigDecimal.valueOf(0);
                    } else if (token.equals("+")) {
                        stack.push(operand1.add(operand2));
                    } else if (token.equals("-")) {
                        stack.push(operand1.subtract(operand2));
                    }
                }
            }
            if (stack.size() != 1)
                return BigDecimal.valueOf(0);
            return stack.pop();
        } else return BigDecimal.valueOf(0);
    }

    /**
     * Проверяет, является ли строка числом
     * @param strNum
     * @return
     */
    public static boolean isNum(String strNum) {
        try {
            BigDecimal bigDecimal = new BigDecimal(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Округление результата
     * @param result
     * @return
     */
    private String roundResult(String result){
        if(result.isEmpty()) return null;
        BigDecimal num = new BigDecimal(result);
        BigDecimal roundOff;
        String [] str = result.split("\\.");
        if(str.length>1){
            if(str[1].length()>4) {
                String res = str[1].subSequence(0,4).toString();
                if(res.equals("0000")) return str[0];
                else {
                    roundOff = num.setScale(4, BigDecimal.ROUND_HALF_EVEN);
                    return printResult(roundOff);
                }
            }
            else if(str[1].equals("0000")) return str[0];
            else return result;
        } else return result;
    }

    /**
     * Удаление нулей
     * @param roundOff
     * @return
     */
    private static String printResult(BigDecimal roundOff) {
        if((roundOff.multiply(new BigDecimal(10000))).remainder(new BigDecimal(10)).compareTo(BigDecimal.ZERO)==0) {
            roundOff=roundOff.setScale(3, RoundingMode.HALF_EVEN);
            if ((roundOff.multiply(new BigDecimal(1000))).remainder(new BigDecimal(10)).compareTo(BigDecimal.ZERO) == 0) {
                roundOff=roundOff.setScale(2, RoundingMode.HALF_EVEN);
                if ((roundOff.multiply(new BigDecimal(100))).remainder(new BigDecimal(10)).compareTo(BigDecimal.ZERO) == 0) {
                    roundOff=roundOff.setScale(1, RoundingMode.HALF_EVEN);
                    if ((roundOff.multiply(new BigDecimal(10))).remainder(new BigDecimal(10)).compareTo(BigDecimal.ZERO) == 0)
                        roundOff=roundOff.setScale(0, RoundingMode.HALF_EVEN);
                }
            }
        }
        return roundOff.toString();
    }
}
